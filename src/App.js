import React from 'react';
import Titles from './components/Titles/Titles';
import Form from './components/Form/Form';
import Weather from './components/Weather/Weather';

const API_KEY = "4d2afba33cbe9cc19a83b913b9b0fa2e";
 class App extends React.Component {
   state = {
     temperature: undefined,
     city: undefined,
     country: undefined,
     humidity: undefined,
     description: undefined,
     isloading: false,
     error: undefined
     
   }
   getWeather = async (e) => {
     e.preventDefault();
     const city = e.target.elements.city.value;
     const country = e.target.elements.country.value;
     const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metrics`);
      const data = await api_call.json();
      if(city && country){
        this.setState((prevState) => ({
          temperature: data.main.temp,
          city: data.name,
          country: data.sys.country,
          humidity: data.main.humidity,
          description: data.weather[0].description,
          isloading: !prevState.isloading,
          error: ''
        }));
      } else {
        this.setState ((prevState) => ({
          temperature: undefined,
          city: undefined,
          country: undefined,
          humidity: undefined,
          description: undefined,
          isloading: prevState.isloadin,
          error: 'Please enter city and country name'
        }));
      }
      
   }

   render() {
    return (
      <div>
        <Titles />
        <Form getWeather={this.getWeather}/>
        <Weather 
         temperature={this.state.temperature}
         city={this.state.city}
         country={this.state.country}
         humidity={this.state.humidity}
         description={this.state.description}
         isloading={this.state.isloading}
         error={this.state.error}/>
      </div>
    );
   }
 };

 export default App;