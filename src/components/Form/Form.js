import React from 'react';
import {  Button, TextField } from '@material-ui/core';
import './Form.css';

class Form extends React.Component {
    render() {
        return (
            <div align="center" className='form'>
                <form onSubmit={this.props.getWeather}>
                  
                        <TextField id="city" label="City" variant="outlined" />
                        <div className="divider" />
                        <TextField id="country" label="Country" variant="outlined" />
                        <div className="divider" />
                        <Button type="submit" variant="outlined" color="primary" >Get Weather</Button>
                  
                </form>
            </div>


        )
    }
}

export default Form;    