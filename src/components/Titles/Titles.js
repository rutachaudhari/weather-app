import React from 'react';
import './Titles.css';
import Box from '@material-ui/core/Box';
 
class Titles extends React.Component {
    render(){
        return(
            <div align="center" className='title'>
                <h1>Weather App</h1>
                <h3 style={{'fontWeight':"normal"}}>Find out temperature, conditions and more...</h3>
            </div>
        );
    }  
}

export default Titles;