import React from 'react';
import { Box, Grid } from '@material-ui/core';
import './Weather.css';

class Weather extends React.Component {
    
    render() {
        console.log(this.props)
        return (
            <div className="root">
                {this.props.isloading ?  <Grid container align="center" className="container">
                    <Box
                        boxShadow={2}
                        bgcolor="lightblue"
                        borderRadius={10}
                        m={1}
                        p={1}
                        style={{ width: '8rem', height: '10rem' }}
                    >
                        {this.props.city && this.props.country && <p>Location: {this.props.city}, {this.props.country}</p>}
                    </Box>
                    <Box
                        boxShadow={2}
                        bgcolor="rgb(243, 166, 136)"
                        borderRadius={10}
                        m={1}
                        p={1}
                        style={{ width: '8rem', height: '10rem' }}
                    >
                        {this.props.temperature && <p>Temperature: {this.props.temperature}</p>}
                    </Box>
                    <Box
                        boxShadow={2}
                        bgcolor="lightblue"
                        borderRadius={10}
                        m={1}
                        p={1}
                        style={{ width: '8rem', height: '10rem' }}
                    >
                        {this.props.humidity && <p> Humidity: {this.props.humidity}</p>}
                    </Box>
                    <Box
                        boxShadow={3}
                        bgcolor="rgb(243, 166, 136)"
                        borderRadius={10}
                        m={1}
                        p={1}
                        style={{ width: '8rem', height: '10rem' }}
                    >
                        {this.props.description && <p>Conditions: {this.props.description}</p>}
                    </Box>
                </Grid>
                :
                <div/>
        }
            </div>
        );
    }
}
export default Weather;